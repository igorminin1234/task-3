package com.example.task3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Task3Application {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Task3Application.class, args);

        HTTPRequester httpRequester = (HTTPRequester) context.getBean("HTTPRequester");

        httpRequester.execute(Constants.FIRST_LINK, Constants.TODAY_AS_DATE);
        httpRequester.execute(Constants.SECOND_LINK, Constants.TODAY_AS_STRING);
    }

}
