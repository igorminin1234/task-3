package com.example.task3;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HTTPRequester {

    public void execute(String link, String date){
        try {
            showMessage(link,getNumberOfArticlesByDate(link, date));
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void showMessage(String link, int numberOfArticles) {
        System.out.println(link + " site has " + numberOfArticles + " articles today");
    }

    public static HttpRequest sendRequest(String link) {
        return HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(link))
                .setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
                .build();
    }

    public static String getHTMLFromResponse(String link) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        return client.send(sendRequest(link), HttpResponse.BodyHandlers.ofString()).body();
    }

    public static int getNumberOfArticlesByDate(String link, String regexp) throws IOException, InterruptedException {
        String html = getHTMLFromResponse(link);
        Pattern pat = Pattern.compile(regexp);
        int numberOfArticles = 0;
        if (html != null) {
            Matcher match = pat.matcher(html);
            while (match.find()) {
                numberOfArticles++;
            }
        }
        return numberOfArticles;
    }

}
