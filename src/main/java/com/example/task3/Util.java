package com.example.task3;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Util {
    public static String changeDateFormat(LocalDate startDateFormat) {
        return new SimpleDateFormat("dd.MM.yyyy")
                .format(Date.from(startDateFormat.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    }
}
