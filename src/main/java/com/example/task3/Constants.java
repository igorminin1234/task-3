package com.example.task3;

import java.time.LocalDate;

public class Constants {
    public static final String TODAY_AS_DATE = "<td class=tdate>" + Util.changeDateFormat(LocalDate.now()).replaceAll("\\.", "\\.") + "<\\/td>";
    public static final String TODAY_AS_STRING = "Разместил\\: [<\\p{L}\\p{P}\\s\\d>=]+Сегодня,";
    public static final String FIRST_LINK = "https://www.opennet.ru/opennews/mini.shtml";
    public static final String SECOND_LINK = "https://mirknig.su/";
}
